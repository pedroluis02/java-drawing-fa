# Drawing Finite Automata
A simple UI for drawing Finite Automata (FA) on Java

#### Drawing
* Draw states.
* Move states inside canvas.
* Draw transitions and labels.
* Set first state (Right-click inside state).
* Set final states (Right-click inside state).
