/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package automata;

/**
 *
 * @author Pedro Luis
 */
import java.util.ArrayList;
import java.util.HashMap;

public class Automata {

    public HashMap<Integer, Estado> estados;
    public ArrayList<Simbolo> alfabeto;
    public int estadoInicial;
    public ArrayList<Integer> estadosFinales;
    public HashMap<String, Transicion> transiciones;

    public Automata() {
        this.estados = new HashMap<>();
        this.alfabeto = new ArrayList<>();
        this.estadoInicial = -1;
        this.estadosFinales = new ArrayList<>();
        this.transiciones = new HashMap<>();
    }

    public int existeInicial() {
        return estadoInicial;
    }

    public void eliminarEstado(int i) {
        if (i >= 0 || i < estados.size()) {
            this.estados.remove(i);
        }
    }

    // buscar simbolo en el alfabeto
    public int existeSimbolo(String s) {
        int i = 0;
        while (i < alfabeto.size()) {
            Simbolo aux = alfabeto.get(i);
            if (s.compareTo(aux.getS()) == 0) {
                return aux.getPos();
            }
            i++;
        }
        return -1;
    }

    // estados
    public void addEstado(int k, Estado e) {
        estados.put(k, e);
    }

    public Estado getEstado(int k) {
        return estados.get(k);
    }

    public boolean existeEstado(int k) {
        return estados.containsKey(k);
    }

    // transiciones
    public void addTransicion(String k, Transicion t) {
        transiciones.put(k, t);
    }

    public Transicion getTransicion(String k) {
        return transiciones.get(k);
    }

    public boolean existeTransicion(String k) {
        return transiciones.containsKey(k);
    }

    // nuevo automata
    public void nuevoAtomata() {
        this.estadoInicial = -1;
        this.estados.clear();
        this.estadosFinales.clear();
        this.alfabeto.clear();
        this.transiciones.clear();
    }
}
