/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package automata;

import java.util.ArrayList;

/**
 *
 * @author Pedro Luis
 */
public class Alfabeto {

    private ArrayList<Simbolo> alf;

    public Alfabeto() {
        alf = new ArrayList<>();
    }

    //----------------------------------
    public void addSimbolo(String s) {
        alf.add(new Simbolo(alf.size(), s));
    }

    //-----------------------------------
    public Simbolo getSimbolo(int pos) {
        return alf.get(pos);
    }

    public int getSize() {
        return alf.size();
    }

    //-----------------------------------
    public int existeSimbolo(String s) {
        int i = 0;
        while (i < alf.size()) {
            if (s.compareTo(alf.get(i).getS()) == 0) {
                return i;
            }
            i++;
        }
        return -1;
    }

    //-----------------------------------
    public void clear() {
        alf.clear();
    }
}
