/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package automata;

/**
 *
 * @author Pedro Luis
 */
public class Simbolo {

    private int pos;
    private String se;

    public Simbolo(int pos, String se) {
        this.pos = pos;
        this.se = se;
    }

    //-----------------------------------
    public String getS() {
        return se;
    }

    public int getPos() {
        return pos;
    }
}
