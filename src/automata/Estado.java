/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package automata;

import java.util.ArrayList;

/**
 *
 * @author pedroluis
 */
public class Estado {

    private int posId;
    private String nombre;

    private ArrayList<String> transiciones; 

    public Estado(int posId, String nombre) {
        this.posId = posId;
        this.nombre = nombre;
        this.transiciones = new ArrayList<>();
    }

    public int getPosId() {
        return posId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setPosId(int posId) {
        this.posId = posId;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void addIdTransicion(String tId) {
        transiciones.add(tId);
    }

    public ArrayList<String> getTransiciones() {
        return transiciones;
    }
}
