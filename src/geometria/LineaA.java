/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geometria;

import constantes.CA;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 *
 * @author pedroluis
 */
public class LineaA extends Line2D.Double {

    public static final int FLECHA_NINGUNA = 9;
    public static final int FLECHA_INICIO = 10;
    public static final int FLECHA_FINAL = 11;

    private int flecha_lugar;
    private Polygon flecha;

    public LineaA(Point2D pa1, Point2D pa2) {
        super(pa1, pa2);
        flecha_lugar = FLECHA_NINGUNA;
        flecha = new Polygon();
    }

    public double getDx1() {
        return getX1() - getX2();
    }

    public double getDy1() {
        return getY1() - getY2();
    }

    public double getDx2() {
        return getX2() - getX1();
    }

    public double getDy2() {
        return getY2() - getY1();
    }

    public void setFlecha(int flecha_lugar) {
        this.flecha_lugar = flecha_lugar;
        if (flecha_lugar == FLECHA_INICIO) {
            flecha(getDx1(), getDy1());
        } else if (flecha_lugar == FLECHA_FINAL) {
            flecha(getDx2(), getDy2());
        }
    }

    public double longitud() {
        return Math.sqrt(Math.pow(getX1() - getX2(), 2)
                + Math.pow(getY1() - getY2(), 2));
    }

    public double getAngulo() {
        double angulo = Math.acos(getDx2() / longitud());
        if (getDy2() >= 0.0) {
            angulo = 6.283185307179586D - angulo;
        }
        return angulo;
    }

    public double getAngulo(double dx, double dy) {
        double angulo = Math.acos(dx / longitud());
        if (dy >= 0.0) {
            angulo = 6.283185307179586D - angulo;
        }
        return angulo;
    }

    public void ajustar() {
        double longitud = longitud();
        Point2D p1, p2;
        if (longitud > (double) CA.estado.EX) {
            Point aux = new Point();
            aux.setLocation((getDx2() * CA.estado.RADIO) / longitud,
                    (getDy2() * CA.estado.RADIO) / longitud);

            p1 = new Point2D.Double(getX1() + aux.x, getY1() + aux.y);
            p2 = new Point2D.Double(getX2() - aux.x, getY2() - aux.y);
        } else {
            p1 = p2 = new Point2D.Double(getX1(), getY1());
        }
        setLine(p1, p2);

        setFlecha(flecha_lugar);
    }

    private void flecha(double dx, double dy) {
        double angulo = getAngulo(dx, dy);
        Point f1 = new Point();
        f1.setLocation(getX2() + Math.sin(angulo
                - 1.047197551196598D) * CA.trans.ARROW_SIZE, getY2()
                + Math.cos(angulo - 1.047197551196598D) * CA.trans.ARROW_SIZE);

        Point f2 = new Point();
        f2.setLocation(getX2() + Math.sin(angulo
                - CA.geo.PI + 1.047197551196598D) * CA.trans.ARROW_SIZE,
                getY2() + Math.cos(angulo - CA.geo.PI
                        + 1.047197551196598D) * CA.trans.ARROW_SIZE);

        if (flecha.npoints > 0) {
            flecha.reset();
        }
        flecha.addPoint((int) getX2(), (int) getY2());
        flecha.addPoint(f1.x, f1.y);
        flecha.addPoint(f2.x, f2.y);
    }

    public void draw(Graphics2D g) {
        g.draw(this);
        if (flecha_lugar != FLECHA_NINGUNA) {
            g.fillPolygon(flecha);
        }
    }

    public Point2D getPuntoMedio() {
        return new Point2D.Double((x1 + x2) / 2,
                (y1 + y2) / 2);
    }
}
