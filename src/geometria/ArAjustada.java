/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geometria;

import constantes.CA;
import java.awt.Graphics2D;
import java.awt.geom.Arc2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

/**
 *
 * @author pedroluis
 */
public class ArAjustada extends Path2D.Double {

    public static final int ARCO = 20;
    public static final int LINEA = 22;
    public static final int LINEA_QUEBRADA = 23;

    private Arc2D arco;
    private LineaA linea;
    private LineaA linea_q;

    private int tipo;
    private boolean lonA; // la arista es quebrada y los estados se superponen

    public ArAjustada(Point2D pi, Point2D pf, int tipo) {
        this.tipo = tipo;
        if (tipo == ARCO) {
            arco = new Arc2D.Double(pi.getX(), pi.getY() - 15,
                    CA.estado.EX, CA.estado.EY, -15, 210, Arc2D.OPEN);
        } else if (tipo == LINEA) {
            linea = new LineaA(pi, pf);
            linea.ajustar();
            linea.setFlecha(LineaA.FLECHA_FINAL);
        } else if (tipo == LINEA_QUEBRADA) {
            lonA = true;
            lineaQuebrada(pi, pf);
        }
    }

    private void lineaQuebrada(Point2D pi, Point2D pf) {

        LineaA lprincipal = new LineaA(pi, pf);
        lprincipal.ajustar();
        if (lprincipal.longitud() < 1.0) {
            linea = new LineaA(pf, pf);
            lonA = false;
            return;
        }

        Point2D puMedio = lprincipal.getPuntoMedio();

        double anElevacion = lprincipal.getAngulo();
        // punto medio elevacion
        Point2D puElevacion = new Point2D.Double(puMedio.getX()
                + Math.sin(anElevacion - CA.geo.PI) * CA.trans.TRANSITION_SIZE,
                puMedio.getY() + Math.cos(anElevacion - CA.geo.PI) * CA.trans.TRANSITION_SIZE);

        linea_q = new LineaA(lprincipal.getP1(), puElevacion);

        /// punto auxiliar medio de elevacion
        Point2D puElevacion_aux = new Point2D.Double(puMedio.getX()
                + Math.sin(anElevacion - CA.geo.PI) * CA.trans.FM_SIZE, puMedio.getY()
                + Math.cos(anElevacion - CA.geo.PI) * CA.trans.FM_SIZE);

        LineaA laux = new LineaA(puElevacion_aux, pf);
        laux.ajustar();

        linea = new LineaA(puElevacion, laux.getP2());
        linea.setFlecha(LineaA.FLECHA_FINAL);
        lonA = true;
    }

    public Point2D getPuntoMedio() {
        if (tipo == ARCO) {
            return new Point2D.Double(arco.getBounds2D().getX(),
                    arco.getBounds2D().getY());
        } else if (tipo == LINEA) {
            return linea.getPuntoMedio();
        } else {
            return linea.getP1();
        }
    }

    public void draw(Graphics2D g) {
        if (tipo == ARCO) {
            g.draw(arco);
        } else if (tipo == LINEA) {
            linea.draw(g);
        } else if (tipo == LINEA_QUEBRADA) {
            if (lonA) {
                linea_q.draw(g);
            }
            linea.draw(g);
        }
    }

    public int getTipo() {
        return tipo;
    }
}
