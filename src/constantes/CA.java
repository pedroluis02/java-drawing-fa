/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package constantes;

/**
 *
 * @author pedroluis
 */
import java.awt.Color;
import java.awt.Font;

public final class CA {

    public static final int NONE = 0;

    public static final class estado {

        //Ex y Ey
        public static final int EX = 50;
        public static final int EY = 50;
        public static final int RADIO = 25;

        //cadena de fuente
        public static final int NAME_SIZE = 11;

        public static final Font FONT_S
                = new Font(Font.SANS_SERIF, Font.BOLD, NAME_SIZE);

        //colores
        public static final Color COLOR_BORDER = Color.BLUE;
        public static final Color COLOR_FILL = Color.YELLOW;
        public static final Color COLOR_FONT = Color.BLACK;
    }

    public static final class canvas {

        //color de fondo del lienzo
        public static final Color BACKGROUND_COLOR = Color.WHITE;
    }

    public static final class trans {

        public static final int TRANSITION_SIZE = estado.RADIO + 15;
        public static final Color CURVE_COLOR = Color.RED;
        public static final Color LABEL_COLOR = Color.BLACK;
        public static final Color PRE_COLOR = Color.GREEN;

        public static final double ARROW_SIZE = 10;
        public static final double FM_SIZE = estado.EX + 20;

        public static final double MIN_SIZE = 10;

        public static final Font FONT_T
                = new Font(Font.SANS_SERIF, Font.BOLD, estado.NAME_SIZE);
    }

    public static final class geo {

        public static final double PI
                = 3.14159265358979323846264338327950288419717D;
    }

    public static final class active {

        public static final int BTNSTATE = 1;
        public static final int BTNTRANS = 2;
    }
}
