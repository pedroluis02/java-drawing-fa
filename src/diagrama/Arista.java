/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package diagrama;

/**
 *
 * @author Pedro Luis
 */
import geometria.ArAjustada;
import constantes.CA;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class Arista {

    private int estadoP;
    private String simbolos;
    private int estadoL;
    private ArAjustada curva;
    private Rectangle2D rectSimbolos;

    public Arista(int estadoP, String simbolos,
            int estadoL, int tipo, Diagrama di) {
        this.estadoP = estadoP;
        this.simbolos = simbolos;
        this.estadoL = estadoL;

        Point2D pi, pf;
        if (tipo == ArAjustada.ARCO) {
            pi = di.estados_gp.get(estadoP).getPunto();
            pf = di.estados_gp.get(estadoL).getPunto();
        } else {
            pi = di.estados_gp.get(estadoP).getCentro();
            pf = di.estados_gp.get(estadoL).getCentro();
        }

        curva = new ArAjustada(pi, pf, tipo);
        rectSimbolos = getRect2DString();
    }

    public void setPuntos(Point2D pai, Point2D paf) {
        curva = new ArAjustada(pai, paf, curva.getTipo());
        rectSimbolos = getRect2DString();
    }

    public void setSimbolos(String simbolos) {
        this.simbolos = simbolos;
        rectSimbolos = getRect2DString();
    }

    public int getEstadoP() {
        return estadoP;
    }

    public String getSimbolos() {
        return simbolos;
    }

    public int getEstadoL() {
        return this.estadoL;
    }

    public void draw(Graphics2D g) {
        g.setColor(CA.trans.CURVE_COLOR);
        curva.draw(g);
        g.setColor(CA.trans.LABEL_COLOR);
        Point2D pm = curva.getPuntoMedio();
        g.setFont(CA.trans.FONT_T);
        g.drawString(simbolos, (int) pm.getX(), (int) pm.getY());
    }

    public void clearDraw(Graphics2D g) {
        g.setColor(CA.canvas.BACKGROUND_COLOR);
        curva.draw(g);
        Point2D pm = curva.getPuntoMedio();
        g.drawString(simbolos, (int) pm.getX(), (int) pm.getY());
    }

    public void setNuevoTipo(int nuevoTipo, Diagrama di) {
        Point2D pi = di.getEstadoGP(estadoP).getCentro();
        Point2D pf = di.getEstadoGP(estadoL).getCentro();
        curva = new ArAjustada(pi, pf, nuevoTipo);
        rectSimbolos = getRect2DString();
    }

    private Rectangle2D getRect2DString() {
        Point2D pm = curva.getPuntoMedio();
        int md = CA.estado.NAME_SIZE / 2;
        int tr = simbolos.length() == 1 ? simbolos.length()
                : Math.round(simbolos.length() / 2);
        return new Rectangle2D.Double(pm.getX() - 4, pm.getY() - md - 6,
                tr * 15, md + 12);
    }

    public boolean puntoDentroSimbolos(double x, double y) {
        return rectSimbolos.contains(x, y);
    }
}
