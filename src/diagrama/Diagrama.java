/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package diagrama;

import java.awt.Point;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author pedroluis
 */
public class Diagrama {

    public HashMap<Integer, EstadoGP> estados_gp;
    public HashMap<String, Arista> aristas;
    public int seleccionado;

    public Diagrama() {
        estados_gp = new HashMap<>();
        aristas = new HashMap<>();
        seleccionado = -1;
    }

    // aritas
    public void addArista(String k, Arista a) {
        aristas.put(k, a);
    }

    public Arista getArista(String k) {
        return aristas.get(k);
    }

    public boolean existeArista(String k) {
        return aristas.containsKey(k);
    }

    // estado
    public void addEstadoGP(int k, EstadoGP e) {
        estados_gp.put(k, e);
    }

    public EstadoGP getEstadoGP(int k) {
        return estados_gp.get(k);
    }

    //
    public int idEstadoActual(Point p) {
        Iterator<EstadoGP> it = estados_gp.values().iterator();
        EstadoGP aux;
        while (it.hasNext()) {
            aux = it.next();
            if (aux.puntoDentro(p.x, p.y)) {
                return aux.getPosId();
            }
        }
        return -1;
    }

    public Arista idAristaActual(Point p) {
        Iterator<Arista> it = aristas.values().iterator();
        Arista aux;
        while (it.hasNext()) {
            aux = it.next();
            if (aux.puntoDentroSimbolos(p.x, p.y)) {
                return aux;
            }
        }
        return null;
    }

}
