package graficadoraf;

/**
 * @author Pedro Luis
 */
import automata.Automata;
import automata.Estado;
import automata.Simbolo;
import automata.Transicion;
import constantes.CA;
import diagrama.Arista;
import diagrama.Diagrama;
import diagrama.EstadoGP;
import geometria.ArAjustada;
import geometria.LineaA;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.MouseInputListener;

public class AreaGrafico extends JPanel implements MouseInputListener,
        ActionListener {

    private JFrame parent;
    private JTextArea TAderivacion = new JTextArea();
    // -- diagrama - automata
    private Automata af; // automata
    private Diagrama di; // diagrama
    // --
    private Point punto,
            puntoRef; // punto referencia para linea de transicion previa.
    private int izq, der, // puntos de # de estado por boton de raton.
            prioEstado; // priorizacion de un estado por superposion de otro.
    private int numEstado, numSimbolo;
    private int btnActivado; // accion[NUEVO_ESTADO | TRANSICION | NINGUNA]
    // grafico
    private Graphics2D graphics;
    //private Image image;

    // Toolbar
    private JToolBar barra;
    private JButton newEstado;
    private JButton newTransicion;

    private JList listaSimbolos;
    private DefaultListModel modelo_ls;

    // dialog
    private JDialog propiedadesEs;
    private JPopupMenu menucontextEs;
    private JMenuItem[] menuitensEs;
    private JCheckBoxMenuItem[] cboxsEs;
    // etiqueta
    JPopupMenu menuContextEti;
    JMenuItem[] menuitensEti;

    public AreaGrafico(final JFrame parent) {
        this.parent = parent;
        //this.parentState = parent.getState();
        af = new Automata();
        di = new Diagrama();
        this.initComponentsDiag();
        this.initMenuContextualEs();
        this.initMenuContextualEti();
        this.setPreferredSize(getScreen());
    }

    private void initComponentsDiag() {
        this.punto = new Point(-1, -1);
        this.puntoRef = new Point(-1, -1);
        this.izq = -1;
        this.der = -1;
        this.prioEstado = -1;
        this.numEstado = 0;
        this.numSimbolo = 0;
        this.btnActivado = CA.NONE;
        this.setBackground(CA.canvas.BACKGROUND_COLOR);
        this.setForeground(CA.canvas.BACKGROUND_COLOR);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    private void initMenuContextualEs() {
        menucontextEs = new JPopupMenu();
        cboxsEs = new JCheckBoxMenuItem[2];
        cboxsEs[0] = new JCheckBoxMenuItem("Inicial");
        cboxsEs[0].setActionCommand("inicial");
        cboxsEs[0].addActionListener(this);

        cboxsEs[1] = new JCheckBoxMenuItem("Final");
        cboxsEs[1].setActionCommand("final");
        cboxsEs[1].addActionListener(this);

        menuitensEs = new JMenuItem[2];
        menuitensEs[0] = new JMenuItem("Eliminar");
        menuitensEs[0].setActionCommand("eliminar");
        menuitensEs[0].addActionListener(this);

        menuitensEs[1] = new JMenuItem("Propiedades");
        menuitensEs[1].setActionCommand("propiedades");

        menucontextEs.add(cboxsEs[0]);
        menucontextEs.add(cboxsEs[1]);
        menucontextEs.add(menuitensEs[0]);
        menucontextEs.add(menuitensEs[1]);

        add(menucontextEs);
    }

    private void initMenuContextualEti() {
        menuContextEti = new JPopupMenu();
        menuitensEti = new JMenuItem[1];

        menuitensEti[0] = new JMenuItem("Editar");
        menuitensEti[0].setActionCommand("editar");
        menuitensEti[0].addActionListener(this);

        menuContextEti.add(menuitensEti[0]);

        add(menuContextEti);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        graphics = (Graphics2D) g;
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        paintAutomata(graphics);
    }

    /* --- graphics --- */
    private void paintAutomata(Graphics2D g) {
        Iterator<EstadoGP> it = di.estados_gp.values().iterator();
        while (it.hasNext()) {
            EstadoGP e = it.next();
            if (prioEstado == e.getPosId()) {
                continue;
            }
            e.draw(g);
            paintAristas(g, e.getIdAristas());
        }
        if (prioEstado != -1) {
            EstadoGP es = di.estados_gp.get(prioEstado);
            es.draw(g);
            paintAristas(g, es.getIdAristas());
        }
        if (puntoRef.x != -1 && btnActivado == CA.active.BTNTRANS) {
            punto = getPointTransPrev(di.estados_gp.get(der).getPunto(),
                    puntoRef);
            paintAristaPrevia(g, punto, puntoRef, false);
        }
    }

    private void estadoMovido(Point p, int pos) {
        EstadoGP es = di.getEstadoGP(pos);
        //es.clearDraw(g);
        p.setLocation(p.x - CA.estado.RADIO, p.y - CA.estado.RADIO);
        es.setPunto(p);
    }

    private void paintAristaPrevia(Graphics2D g, Point pi, Point pf,
            boolean clean) {
        if (clean) {
            g.setColor(CA.canvas.BACKGROUND_COLOR);
        } else {
            g.setColor(CA.trans.PRE_COLOR);
        }
        g.drawLine(pi.x, pi.y, pf.x, pf.y);
    }

    private void paintArista(int ei, int ef) {

        String simbolo
                = JOptionPane.showInputDialog(parent, "Simbolo");
        if (simbolo == null) {
            return;
        }

        if (simbolo.isEmpty()) {
            return;
        }

        int posS = af.existeSimbolo(simbolo);
        if (posS == -1) {
            af.alfabeto.add(new Simbolo(numSimbolo, simbolo));
            posS = numSimbolo;
            numSimbolo++;
            modelo_ls.addElement(simbolo);
        }

        if (posS != -1) {
            if (af.existeTransicion(ei + "-" + ef)) {
                Transicion t = af.getTransicion(ei + "-" + ef);
                if (t.existeSimbolo(posS)) {
                    JOptionPane.showMessageDialog(parent, "Transición con '"
                            + simbolo + "' ya existe.",
                            "Agregar Transición", JOptionPane.WARNING_MESSAGE);
                } else {
                    t.addNuevoSimbolo(posS);
                    Arista a = di.getArista(ei + "-" + ef);
                    a.setSimbolos(a.getSimbolos() + "," + simbolo);
                }
            } else {
                Transicion tn = new Transicion(ei, posS, ef);
                af.addTransicion(ei + "-" + ef, tn);
                af.estados.get(ei).addIdTransicion(ei + "-" + ef);

                int tipo = ArAjustada.LINEA;
                if (ei == ef) {
                    tipo = ArAjustada.ARCO;
                } else if ((ei != ef)
                        && di.existeArista(ef + "-" + ei)) {
                    tipo = ArAjustada.LINEA_QUEBRADA;
                    Arista aux = di.getArista(ef + "-" + ei);
                    //aux.clearDraw(graphics);
                    aux.setNuevoTipo(ArAjustada.LINEA_QUEBRADA, di);
                }

                Arista an = new Arista(ei, simbolo, ef, tipo, di);
                di.addArista(ei + "-" + ef, an);
                di.estados_gp.get(ei).addIdArista(ei + "-" + ef);

                if (ei != ef) {
                    af.estados.get(ef).addIdTransicion(ei + "-" + ef);
                    di.estados_gp.get(ef).addIdArista(ei + "-" + ef);
                }
            }
        } else {
            JOptionPane.showMessageDialog(parent, "Símbolo '"
                    + simbolo + "' no existe.",
                    "Agregar Transición", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void paintAristas(Graphics2D g, ArrayList<String> eat) {
        int j = 0;
        Arista ar;
        while (j < eat.size()) {
            ar = di.getArista(eat.get(j));
            ar.draw(g);
            j++;
        }
    }

    private void aristasMovidas(int pos) {
        ArrayList<String> eat = di.estados_gp.get(pos).getIdAristas();
        int j = 0;
        Arista ar;
        while (j < eat.size()) {
            ar = di.getArista(eat.get(j));
            //ar.clearDraw(g);
            if (ar.getEstadoL() == ar.getEstadoP()) {
                ar.setPuntos(di.getEstadoGP(pos).getPunto(),
                        di.getEstadoGP(pos).getPunto());
            } else {
                String bs = eat.get(j);
                int pos_r = bs.indexOf("-");
                Point2D aux;

                if (bs.substring(0, pos_r).compareTo("" + pos) == 0) {
                    aux = di.getEstadoGP(ar.getEstadoL()).getCentro();
                    ar.setPuntos(di.getEstadoGP(pos).getCentro(), aux);
                } else {
                    aux = di.getEstadoGP(pos).getCentro();
                    ar.setPuntos(di.getEstadoGP(ar.getEstadoP()).getCentro(),
                            aux);
                }
            }
            j++;
        }
    }

    /* --- Events --- */
    @Override
    public void mouseDragged(MouseEvent e) {
        if (btnActivado == CA.active.BTNTRANS) {
            if (der == -1) {
                btnActivado = CA.NONE;
                return;
            }
            paintAristaPrevia(graphics, punto, puntoRef, true);
            puntoRef = e.getPoint();
            repaint();
        } else {
            if (izq == -1) {
                btnActivado = CA.NONE;
                return;
            }
            estadoMovido(e.getPoint(), izq);
            if (!di.estados_gp.get(izq).getIdAristas().isEmpty()) {
                aristasMovidas(izq);
            }
            repaint();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            if (btnActivado == CA.active.BTNSTATE) {
                Point p = e.getPoint();
                p.translate(-CA.estado.RADIO, -CA.estado.RADIO);
                af.addEstado(numEstado, new Estado(numEstado, "q" + numEstado));
                di.addEstadoGP(numEstado,
                        new EstadoGP(numEstado, p, "q" + numEstado));
                repaint();
                numEstado++;
                btnActivado = CA.NONE;
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            if (btnActivado == CA.active.BTNTRANS) {
                der = di.idEstadoActual(e.getPoint());
            } else if (btnActivado == CA.NONE) {
                izq = di.idEstadoActual(e.getPoint());
                prioEstado = izq;
                if (izq != -1) {
                    setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                }
            }
        }

        if (e.isPopupTrigger()) {
            der = di.idEstadoActual(e.getPoint());
            if (der != -1) {
                EstadoGP es = di.getEstadoGP(der);
                menucontextEs.setLabel("Estado: " + es.getNombre());
                if (af.estadoInicial == -1) {
                    cboxsEs[0].setState(false);
                    cboxsEs[0].setEnabled(true);
                } else if (der == af.estadoInicial) {
                    cboxsEs[0].setState(true);
                    cboxsEs[0].setEnabled(true);
                } else {
                    cboxsEs[0].setState(false);
                    cboxsEs[0].setEnabled(false);
                }

                cboxsEs[1].setState(es.isFinal());
                menucontextEs.show(this, e.getX(), e.getY());
                return;
            }

            Arista ar = di.idAristaActual(e.getPoint());
            if (ar != null) {
                menuContextEti.setLabel("Transición \n"
                        + di.getEstadoGP(ar.getEstadoP()).getNombre() + " ==> "
                        + di.getEstadoGP(ar.getEstadoL()).getNombre());
                menuContextEti.show(this, e.getX(), e.getY());
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            if (btnActivado == CA.active.BTNTRANS && der != -1) {
                int ptf = di.idEstadoActual(e.getPoint());
                paintAristaPrevia(graphics, punto, e.getPoint(), true);
                if (ptf != -1) {
                    paintArista(der, ptf);
                }
                repaint();
                btnActivado = CA.NONE;
            } else if (btnActivado == CA.NONE && izq != -1) {
                btnActivado = CA.NONE;
                izq = -1;
            }
            puntoRef.setLocation(-1, -1);
            setCursor(Cursor.getDefaultCursor());
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
        /*if(btnActivado == CA.active.BTNSTATE) {
            izq = -1;
            setCursor(Cursor.getDefaultCursor());
            btnActivado = CA.NONE;
        }else*/ if (btnActivado == CA.active.BTNTRANS) {
            setCursor(Cursor.getDefaultCursor());
            der = -1;
            btnActivado = CA.NONE;
            paintAristaPrevia(graphics, punto, puntoRef, true);
            repaint();
        }
    }

    /* --- functions 1 --- */
    private void nuevoDiagrama() {
        this.af.nuevoAtomata();
        this.izq = -1;
        this.der = -1;
        this.numEstado = 0;
        this.punto.move(-1, -1);
        this.puntoRef.move(15, 5);
        this.TAderivacion.setText("");
        this.repaint();
    }

    public void removeEstado(Graphics2D g, int pos) {
        EstadoGP es = di.getEstadoGP(pos);
        //es.clearDraw(g);
        eliminarEstado(pos);
        numEstado--;
    }

    public void eliminarEstado(int estado) {
        EstadoGP es = di.getEstadoGP(estado);

        int i = 0;
        String aux;
        EstadoGP aux_es;
        while (i < es.getIdAristas().size()) {
            aux = es.getIdAristas().get(i);
            int e_1 = Integer.parseInt(aux.split("-")[0]);
            int e_2 = Integer.parseInt(aux.split("-")[1]);

            if (e_1 != e_2) {
                if (e_1 == estado) {
                    aux_es = di.getEstadoGP(e_2);
                } else {
                    aux_es = di.getEstadoGP(e_1);
                }
                aux_es.getIdAristas().remove(e_1 + "-" + e_2);
            }

            af.transiciones.remove(e_1 + "-" + e_2);
            di.aristas.remove(e_1 + "-" + e_2);

            i++;
        }
        af.eliminarEstado(estado);
        di.estados_gp.remove(estado);
    }

    /* --- left components --- */
    private void DialogoPro(final Point p, int _pos) {
        String name = di.estados_gp.get(_pos).getNombre();
        boolean is = di.estados_gp.get(_pos).isInicial();
        boolean ls = di.estados_gp.get(_pos).isFinal();
        final JDialog dialogo = new JDialog(new JFrame(), "Estado " + name);
        final JCheckBox RBinicial = new JCheckBox("Inicial");
        RBinicial.setSelected(is);
        final JCheckBox RBfinal = new JCheckBox("Final");
        RBfinal.setSelected(ls);
        final JCheckBox RBquitar = new JCheckBox("Quitar");
        JButton aceptar = new JButton("Aceptar");
        dialogo.setLocation(p);
        dialogo.setSize(200, 120);
        dialogo.setLayout(new java.awt.FlowLayout());
        dialogo.add(BorderLayout.CENTER, RBinicial);
        dialogo.add(BorderLayout.CENTER, RBfinal);
        dialogo.add(BorderLayout.CENTER, RBquitar);
        dialogo.add(BorderLayout.SOUTH, aceptar);
    }

    public JPanel getBarraBotones() {
        modelo_ls = new DefaultListModel<String>();
        listaSimbolos = new JList(modelo_ls);
        listaSimbolos.setFixedCellWidth(50);
        listaSimbolos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listaSimbolos.setBorder(BorderFactory.createTitledBorder("Alfabeto"));

        newEstado = new JButton("Nuevo Estado");
        newEstado.setActionCommand("newestado");
        newEstado.addActionListener(this);
        newTransicion = new JButton("Nueva Transición");
        newTransicion.setActionCommand("newtrans");
        newTransicion.addActionListener(this);

        GridBagLayout g = new GridBagLayout();
        JPanel panelIzquierdo = new JPanel(g);
        panelIzquierdo.setBorder(
                BorderFactory.createEmptyBorder(5, 5, 5, 5));
        GridBagConstraints pos = new GridBagConstraints();

        GridLayout grid = new GridLayout(2, 1);
        grid.setVgap(10);
        JPanel pan = new JPanel(grid);
        pan.add(newEstado);
        pan.add(newTransicion);

        pos.gridx = 0;
        pos.gridy = 0;
        pos.gridheight = 1;
        pos.gridwidth = 1;
        pos.fill = 1;
        pos.weightx = 1;
        panelIzquierdo.add(pan, pos);

        pos.gridx = 0;
        pos.gridy = 1;
        panelIzquierdo.add(new JToolBar.Separator(), pos);

        pos.gridx = 0;
        pos.gridy = 2;
        pos.gridheight = 1;
        pos.gridwidth = 1;
        pos.weighty = 1;
        pos.fill = 1;
        JScrollPane pane = new JScrollPane(listaSimbolos);
        panelIzquierdo.add(pane, pos);

        return panelIzquierdo;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String accion = ae.getActionCommand();
        if (accion.compareTo("newestado") == 0) {
            btnActivado = CA.active.BTNSTATE;
            setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
        } else if (accion.compareTo("newtrans") == 0) {
            btnActivado = CA.active.BTNTRANS;
            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        } //
        else if (accion.compareTo("inicial") == 0) {
            EstadoGP es = di.getEstadoGP(der);
            es.setInicial(cboxsEs[0].getState());
            if (cboxsEs[0].getState()) {
                af.estadoInicial = der;
            } else {
                af.estadoInicial = -1;
            }
            der = -1;
            repaint();
        } else if (accion.compareTo("final") == 0) {
            di.getEstadoGP(der).setFinal(cboxsEs[1].getState());
            der = -1;
            repaint();
        } //
        else if (accion.compareTo("eliminar") == 0) {
            eliminarEstado(der);
            der = -1;
            repaint();
            System.out.println("dsf");
        } else if (accion.compareTo("propiedades") == 0) {
            System.out.println("propiedades");
        } //
        else if (accion.compareTo("editar") == 0) {
            System.out.println("editar");
        }
    }

    /* --- Other functions --- */
    public static Dimension getScreen() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        return new Dimension(toolkit.getScreenSize());
    }

    private Point getPointTransPrev(Point pi, Point pf) {
        Point p = (Point) pi.clone();
        Point pfaux = (Point) pf.clone();
        p.translate(CA.estado.RADIO, CA.estado.RADIO);
        LineaA auxL = new LineaA(p, pfaux);
        auxL.ajustar();
        return new Point((int) auxL.getX1(), (int) auxL.getY1());
    }
}
