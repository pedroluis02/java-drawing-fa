/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package graficadoraf;

/**
 *
 * @author Pedro Luis
 */
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
//import javax.swing.UIManager;

public class Principal extends JFrame {

    /**
     * @param args the command line arguments
     */
    private AreaGrafico diagrama;

    public Principal(String title) {
        super(title);
        this.init();
    }

    private void init() {
        diagrama = new AreaGrafico(this);
        Dimension d = AreaGrafico.getScreen();
        this.add(BorderLayout.WEST, diagrama.getBarraBotones());

        JScrollPane p = new JScrollPane(diagrama);
        p.setAutoscrolls(false);

        this.add(BorderLayout.CENTER, p);

        this.setSize(d.width - (int) (d.width / 2.5), d.height - d.height / 4);
        //this.setLocation(d.width/4, d.height/8);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        // TODO code application logic here
        boolean success = false;
        //<editor-fold defaultstate="collapsed" desc="Theme System">
        /*try{
            for(UIManager.LookAndFeelInfo info :
                    UIManager.getInstalledLookAndFeels()){
                System.out.println(info.getName());
                if(info.getName().compareTo("Nimbus") == 0){
                    UIManager.setLookAndFeel(info.getClassName());
                    success = true;
                    break;
                }
            } // Metal Nimbus CDE/Motif GTK+
        }catch(Exception ex){
            success = false;
            JOptionPane.showMessageDialog(null,
                    "Error al cargar Tema \n"+ex.getMessage(),
                    "Error de tema",
                    JOptionPane.WARNING_MESSAGE);
        }*/ // </editor-fold>

//        if(success == false){
//            JFrame.setDefaultLookAndFeelDecorated(true);
//            JDialog.setDefaultLookAndFeelDecorated(true);
//        }
        Principal p = new Principal("Graficador de Automatas");
        p.setVisible(true);
    }
}
